<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SendInvitationEmail;
use Illuminate\Support\Facades\Mail;

class SendInvitationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emailaddress;
    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailaddress, $token)
    {
        $this->emailaddress = $emailaddress;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
     
        $email = new SendInvitationEmail($this->token);        
        Mail::to($this->emailaddress)->send($email);

    }
}
