**Invitation Hunter**
---

## Installation and Deploy

You’ll start by following guide in this README file to learn how to use InviteHunter.

1. clone the repository, you can use **git clone URL** or download. Then cd into the folder by typing **cd invitehunter**
2. Run **composer install**
3. Run **touch database/database.sqlite**  this will Create a database file, we use sqlite for quick. Or make a file named database.sqlite on folder database
4. Run **php artisan migrate** 
5. Run **php artisan db:seed**
6. Ready, now run **php artisan serve** then access on the browser (usually http://127.0.0.1:8000).
7. Addition, you may run **php artisan queue:listen** to listen for any queue
8. Change the SMTP server on .env file, currently we still use our mailtrap account, but its free to use any 

---

## TESTING

to check if all function run properly 
**php artisan test**

---
---

## How To Use

Next, you’ll add a new file to this repository.

1. Click on **invitation** menu
2. Click **Add New Invitation** to add new email invitation.
3. By default it will not sent the email yet, so you can review the list
4. Once you are OK, click **Send All Invitation** to send all email to invitee
5. You can see the preview by clicking on Token Name

---

## CONFIGURATION

Next, you may want to change some thing here

1. Open **config/invitationhunter.php** for basic config like deadline date, or event date
2. For Email Delay, change number on **emaildelay** on config to any number you want


---

## CONTACT

1. MAILME AT tonitegarsahidi [at] gmail [dot] com
2. Whatsapp Me at +62 852 0173 8000


---
