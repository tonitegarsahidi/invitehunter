<?php

namespace App\Http\Controllers;

use App\Jobs\SendConfirmationJob;
use App\Jobs\UpdateStatusJob;
use App\Models\InvitationData;
use App\Models\InvitationLetter;
use App\Models\InvitationPreferences;
use App\Models\Preferences_item;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;

class InvitationDataController extends Controller
{

    public function validateRequest(){

        // dd(request());
        return request()->validate([
            'name' => 'required',
            'dob' => 'required|date',
            'gender' => 'required|numeric',
            'preferences' => 'required|array',
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($token)
    {

        $result = InvitationLetter::where('token',$token)->first();

        $data = $this->validateRequest();
        $data["invl_id"] = $result->id;

        //update the status of the invitation
        $result->status_id = 3;

        //this is not token, this is the passed key for him
        //the format can be anything though
        $randomkey = Str::random(5).$token.Str::random(5);

        $result->key = $randomkey;
        

        $result->save();


        


        // check if invitation data is filled or not,a ctually this page wont be called, since the form wont show, however, shit happens sometimes, 
        if(InvitationData::where('invl_id', $result->id)->exists()){
            return redirect('/invitation/'.$token);
        }
        else{
            $invitationData = InvitationData::create([
                "invl_id" => $data["invl_id"],
                "name" => $data["name"],
                "dob" => $data["dob"],
                "gender" => $data["gender"],
            ]);

            foreach($data["preferences"] as $item){

                if(Preferences_item::where('id', $item)->exists()){      
                    InvitationPreferences::create([
                        'invl_id'=>$data['invl_id'],
                        'preferences_item_id' => $item,
                    ]);
                }
        }


        //SEND QUEUE MAIL, THEN UPDATE THE STATUS AFTER THAT

        // dd($result->token, $result->key);

        $delayminute = config('invitationhunter.delayemail');

        // dd($delayminute);
        $status1 = SendConfirmationJob::dispatch($result->email, $result->token, $result->key)->delay(now()->addMinutes($delayminute));
        $status2 = UpdateStatusJob::dispatch($result->id)->delay(now()->addMinutes($delayminute));

            

            return redirect('/invitation/'.$token);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvitationData  $invitationData
     * @return \Illuminate\Http\Response
     */
    public function show(InvitationData $invitationData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvitationData  $invitationData
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitationData $invitationData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvitationData  $invitationData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvitationData $invitationData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvitationData  $invitationData
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitationData $invitationData)
    {
        //
    }
}
