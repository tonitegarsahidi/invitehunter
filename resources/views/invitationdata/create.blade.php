@extends('templateblank')

@section('title')
Admin Invitation Hunter
@endsection

@section('headercode')

@endsection


@section('maincontent')



<div class="py-12 bg-white mx-auto mt-8 mb-8 w-5/6">
    <div class="max-w-6xl px-4 sm:px-6 lg:px-8 mx-auto">
        <div class="lg:text-center">
            <h2 class="text-base text-indigo-600 font-semibold tracking-wide uppercase">Prospect Hunter</h2>
            <p class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                Add New Invitation List
            </p>
            <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                Input Email below
            </p>
        </div>


        {{--  THE DATA GOES HERE  --}}
        <div class="mt-10 mx-auto">

            <div class="px-12 py-12 mx-auto">
                <form action="/invl" method="post">
                
                <div class="mt-8 max-w-md mx-auto">
                  <div class="grid grid-cols-1 gap-6">

                    <label class="block">
                      <span class="text-gray-700">Email address</span>
                      <input
                        type="email"
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                        placeholder="john@example.com"
                      />
                    </label>

                    <label class="block">
                      <span class="text-gray-700">When is your event?</span>
                      <input
                        type="date"
                        class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                      />
                    </label>

                  

                    <div class="block">
                      <div class="mt-2">
                        <div>
                          <label class="inline-flex items-center">
                            <input
                              type="checkbox"
                              class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-offset-0 focus:ring-indigo-200 focus:ring-opacity-50"
                              checked
                            />
                            <span class="ml-2">Email me news and special offers</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                </form>
            </div>
           
        </div>
    </div>
</div>

@endsection

@section('footercode')

@endsection