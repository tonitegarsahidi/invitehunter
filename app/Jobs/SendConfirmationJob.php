<?php

namespace App\Jobs;

use App\Mail\SendConfirmationEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;        
use Carbon\Carbon;

class SendConfirmationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emailaddress;
    protected $token;
    protected $key;
    

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailaddress, $token, $key)
    {
        $this->emailaddress = $emailaddress;
        $this->token = $token;
        $this->key = $key;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendConfirmationEmail($this->token, $this->key);        
        Mail::to($this->emailaddress)->send($email);
    }
}
