<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(["statusname" =>  "Invitation Created"]);
        Status::create(["statusname" =>  "Invitation Sent"]);
        Status::create(["statusname" =>  "Accepted and Filled"]);
        Status::create(["statusname" =>  "Invitation Finish"]);
    }
}
