<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationLetter extends Model
{
    use HasFactory;
    

    protected $guarded = [];
    protected $table = 'invitation_letters';

    public function status(){   //kurang rapi untuk penamaannya, wip later
        return $this->belongsTo('App\Models\Status', 'status_id','id');
    }
    
    public function invitationdata(){   //kurang rapi untuk penamaannya, wip later
        return $this->hasOne('App\Models\InvitationData', 'invl_id','id');
    }

    public function preferences(){
        return $this->hasMany('App\Models\InvitationPreferences', 'invl_id','id');
    }
}
