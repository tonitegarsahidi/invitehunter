<?php

namespace Tests\Feature;

use App\Models\InvitationLetter;
use App\Models\InvitationData;
use App\Models\InvitationPreferences;
use App\Models\Preferences_item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvitationDataTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_invitation_data_can_be_created(){

        // $this->withoutExceptionHandling();

        $this->seed();

        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();

        $this->assertCount(1, InvitationLetter::all());

        $token = InvitationLetter::first()->token;
        $preferences = [1,3,6,8,7];

        //now accept
        $response = $this->post('/invitation/'.$token, [
            'name' => "Puteri Sultanah",
            'dob' => "1987-12-02",
            'gender' => 2,
            'preferences' => $preferences,
        ]); 

        $this->assertCount(count($preferences), InvitationPreferences::all());

    }

    public function test_invitation_data_accepted_cannot_be_changed(){
        // $this->withoutExceptionHandling();

        $this->seed();

        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();

        $this->assertCount(1, InvitationLetter::all());

        $token = InvitationLetter::first()->token;
        
        $preferences = [1,12,11,14,15];

        //now accept
        $response = $this->post('/invitation/'.$token, [
            'name' => "Puteri Sultanah",
            'dob' => "1987-12-02",
            'gender' => 2,
            'preferences' => $preferences,
        ]); 

        // dd($response);
        $this->assertCount(1, InvitationData::all());

        // dd(InvitationPreferences::all());
        $this->assertCount(count($preferences), InvitationPreferences::all());

        //NOW TRY TO CHANGE THE CONTENT

        //now the changed data
        $response = $this->post('/invitation/'.$token, [
            'name' => "Nyonya Sultanah",
            'dob' => "1988-11-22",
            'gender' => 3,
            'preferences' => [1,12,11,14,15,16,19,21],
        ]);

        //check if the database is still inteact, not added
        $this->assertCount(1, InvitationData::all());
        $this->assertCount(count($preferences), InvitationPreferences::all());

        //check if the content is not changed
        $this->assertEquals("Puteri Sultanah", InvitationData::first()->name);
        $this->assertEquals("1987-12-02", InvitationData::first()->dob);
        $this->assertEquals("2", InvitationData::first()->gender);

        
    }
}
