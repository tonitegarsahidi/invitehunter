<?php

namespace Database\Seeders;

use App\Models\Preferences_item;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class Preference_item_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    $topbrand = [
        'Balenciaga',
        'Burberry',
        'Bottega Veneta',
        'Céline',
        'Chanel',
        'Christian Louboutin',
        'Coach',
        'Christian Dior',
        'Fendi',
        'Givenchy',
        'Gucci',
        'Hermès',
        'Lanvin',
        'Longchamp',
        'Louis Vuitton',
        'Michael Kors',
        'Charlotte Olympia',
        'Self Portrait',
        'Tod\'s',
        'Valentino',
    ];



        foreach($topbrand as $item){
            Preferences_item::create(["itemname" =>  $item]);
        }
        
    }
}
