<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class SendConfirmationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     protected $token, $key;

    public function __construct($token, $key)
    {
        $this->key = $key;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->key;    

        $url = URL::to('/invitation/'.$this->token);
        $dateevent = Carbon::createFromFormat('Y-m-d', config('invitationhunter.dateevent'))->format('d-m-Y');
        $datedeadline = Carbon::createFromFormat('Y-m-d', config('invitationhunter.datedeadline'))->format('d-m-Y');
        return $this->subject(config('invitationhunter.confirmationsubject'))
                    ->view('email.confirmation', compact('data', 'dateevent', 'datedeadline', 'url'));
    }
}
