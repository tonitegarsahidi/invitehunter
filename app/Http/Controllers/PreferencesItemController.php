<?php

namespace App\Http\Controllers;

use App\Models\Preferences_item;
use Illuminate\Http\Request;

class PreferencesItemController extends Controller
{
    public function validateRequest(){
        return request()->validate([
            'itemname' => 'required|string',
        ]);

    }

    public function index(){

        $data = Preferences_item::all();
        return view('preferencesitem.index', compact('data'));
    }

      /**
     * Store a newly InvitationLettereated resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Preferences_item::create($this->validateRequest());
    }
}
