<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationData extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $table = 'invitation_data';

    public function setDobAttribute($dob){
        $this->attributes['dob'] = Carbon::createFromFormat("Y-m-d", $dob)->toDateString();
    }

    public function getDobAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    
}
