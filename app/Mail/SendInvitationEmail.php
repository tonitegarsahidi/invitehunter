<?php

namespace App\Mail;

use App\Models\InvitationLetter;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class SendInvitationEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        //

        $this->data = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() 
    {

        $data = $this->data;    

        $url = URL::to('/invitation/'.$data);
        $dateevent = Carbon::createFromFormat('Y-m-d', config('invitationhunter.dateevent'))->format('d-m-Y');
        $datedeadline = Carbon::createFromFormat('Y-m-d', config('invitationhunter.datedeadline'))->format('d-m-Y');
        return $this->subject(config('invitationhunter.invitationsubject'))
                    ->view('email.invitation', compact('data', 'dateevent', 'datedeadline', 'url'));
    }
}
