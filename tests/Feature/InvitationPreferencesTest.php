<?php

namespace Tests\Feature;

use App\Models\InvitationPreferences;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvitationPreferencesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_invitation_preferences_can_be_added(){
        $response = InvitationPreferences::create([
            'invl_id'=> 1,
            'preferences_item_id' => 12,
        ]);

        // $response->assertOK();

        $this->assertCount(1, InvitationPreferences::all());
    }
}
