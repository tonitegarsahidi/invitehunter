<?php

namespace Database\Seeders;

use App\Models\InvitationLetter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class InvitationLetterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvitationLetter::create(["email"=>"toni@huntstreet.com", "token"=>Str::random(10), "status_id"=>1]);
        InvitationLetter::create(["email"=>"tegar@huntstreet.com", "token"=>Str::random(10), "status_id"=>1]);
        InvitationLetter::create(["email"=>"sahidi@huntstreet.com", "token"=>Str::random(10), "status_id"=>1]);
    }
}
