<br>
<div class="bg-red-200 text-center">
  @php
  $datetarget = config('invitationhunter.datedeadline');
  
  $timestamp = strtotime($datetarget);
  $datedisplay = date("l d F Y", $timestamp);
  @endphp

  <h3>This Invitation will expired on {{$datedisplay}}</h3>


  @include('include/countdowntimer')
</div>

<br>
<hr>



@include('include/message')
              <form action="/invitation/{{$data->token}}" method="post">

                @csrf

                <div class="mt-8 max-w-md mx-auto">
                    <div class="grid grid-cols-1 gap-6">
  
                      <label class="block">
                        <span class="text-gray-700">Email</span>
                        <input
                          type="email"
                          name="email"
                          disabled
                          class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          placeholder="your name"
                          value="{{$data->email}}"
                        />
                      </label>
                      
                      <label class="block">
                        <span class="text-gray-700">Your Full Name</span>
                        <input
                          type="text"
                          name="name"
                          {{$disabledform}}
                          class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                          placeholder="your name"
                          value="{{old('name',"")}}"
                        />
                      </label>
  
                      <label class="block">
                        <span class="text-gray-700">Date of Birth?</span>
                        <input
                          type="date"
                          name="dob"
                          {{$disabledform}}
                          value="{{old('dob',"")}}"
                          class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                        />
                      </label>
                      
                      <label class="block">
                        <span class="text-gray-700">I am </span>
                        <select name="gender" id="gender" {{$disabledform}}>
                            <option value="1">Female</option>
                            <option value="2">Male</option>
                            <option value="3">Choose not to answer</option>
                        </select>
                      </label>
                      
                      <label class="block">
                        <span class="text-gray-700">My Favouritte </span>
                        <div class="grid grid-cols-2 gap-4">
                            @foreach ($preferencesitem as $item)
                            <div>
                                <input
                                    name="preferences[]"
                                    type="checkbox"
                                    value="{{$item->id}}"
                                    {{$disabledform}}
                                        class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-offset-0 focus:ring-indigo-200 focus:ring-opacity-50"
                                />
                                <span class="ml-2">{{$item->itemname}}</span>
                            </div>
                            
                            @endforeach
                          </div>
                      </label>
  
                    
  
                      <div class="pt-12 pb-8">
                        <button class="bg-green-700 hover:bg-green-900 text-white font-bold py-2 px-4 rounded-full">
                          Register
                        </button> 
                    </div>
                    </div>
                  </div>

                

              </form>