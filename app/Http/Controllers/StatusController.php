<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index(){
        $data = Status::all();
        return view('status.index',compact('data'));
    }
}
