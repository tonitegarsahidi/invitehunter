<?php

namespace App\Http\Controllers;

use App\Jobs\SendInvitationJob;
use App\Mail\SendInvitationEmail;
use App\Models\InvitationLetter;
use App\Models\Preferences_item;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class InvitationLetterController extends Controller
{

    public function validateRequest(){
        return request()->validate([
            'email' => 'required|email',
            'status_id' => 'numeric|nullable',
        ]);

    }
    
    public function validateRequestUpdate(){
        return request()->validate([
            'email' => 'email|nullable',
            'status_id' => 'numeric|nullable',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=InvitationLetter::all();


        return view('invitationletter.index', compact('data'));
    }

    public function indexConfirm()
    {

        $data=InvitationLetter::where('status_id','>=','3')->get();


        return view('invitationletter.indexconfirm', compact('data'));
    }

    /**
     * Show the form for InvitationLettereating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invitationletter.create');
    }

    /**
     * Store a newly InvitationLettereated resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invitationLetter = $this->validateRequest();
        
        //generating the token, if no request token is provided
        $invitationLetter["token"] = Str::random(10);


        //check if current token is duplicate with already
        //regenerate if token exist
        while(InvitationLetter::where('token',$invitationLetter["token"])->exists()){
            $invitationLetter["token"] = Str::random(10);
        }
        
        $status = InvitationLetter::create($invitationLetter);
        
        $message = $status ? ["color" => "bg-green-100 text-green-800 text-sm", "content"=>"Data ".$invitationLetter["email"]." has been added"] : ["class" => "bg-red-100 text-red-800 text-sm", "text"=>"Ups, DB Error! Data ".$invitationLetter["email"]." failed to be added"];

        $data = InvitationLetter::all();

        return view('invitationletter.index', compact('message', 'data'));
        
    }



    public function sendAllInvitation(){
        $unsentinvitation = InvitationLetter::where('status_id', 1)->get();

        $messagecontent = "<ol>";
        //sent each invitation unsent yet
        foreach($unsentinvitation as $item){
            //SENDING MAIL part is here ==============================

            $status = dispatch(new SendInvitationJob($item->email, $item->token));
           
            // $email = new SendInvitationEmail($item->token);        
            // $status =  Mail::to($item->email)->send($email);
            // dd($status);
            $statussendmail = $status != null; 
            $statusupdate = true;

            if($statussendmail){
                //update status_id
                $item->status_id = 2;
                $statusupdate = $item->save();  //will true if succeed
            }

            if($statusupdate){
                $messagecontent .= "<li>OK - Invitation for ".$item->email." Successfully Sent</li>";
            }
            else{
                $messagecontent .= "<li>ERROR - Invitation for ".$item->email." Failed to be Sent</li>";
            }

        }

        $messagecontent.= "</ol>";


        $message = ["color" => "bg-blue-100 text-blue-800 text-sm", "content"=>$messagecontent]; 
        $data = InvitationLetter::all();
        return view('invitationletter.index', compact('message', 'data'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvitationLetter  $invitationLetter
     * @return \Illuminate\Http\Response
     */
    public function show(InvitationLetter $invitationLetter)
    {
        
        //change for view later
        return "<pre>".$invitationLetter."</pre>";

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvitationLetter  $invitationLetter
     * @return \Illuminate\Http\Response
     */
    public function showinvitation($token)
    {
        $result = InvitationLetter::where('token',$token)->first();
        $preferencesitem = Preferences_item::all();

        $data = $result == null ? abort(404) : $result;
        
        
        //change for view later
        return view('invitationletter.showinvitation', compact('data', 'preferencesitem'));
        return "<h1>".$data->email."</h1>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvitationLetter  $invitationLetter
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitationLetter $invitationLetter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvitationLetter  $invitationLetter
     * @return \Illuminate\Http\Response
     */
    public function update(InvitationLetter $invitationLetter)
    {
        $invitationLetter->update($this->validateRequestUpdate());
        
        return redirect('/invl');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvitationLetter  $invitationLetter
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitationLetter $invitationLetter)
    {
        $status = $invitationLetter->delete();

        $message = $status ? ["color" => "bg-green-100 text-green-800 text-sm", "content"=>"Data ".$invitationLetter["email"]." has been deleted"] : ["class" => "bg-red-100 text-red-800 text-sm", "text"=>"Ups, DB Error! Data ".$invitationLetter["email"]." failed to be deleted"];

        $data = InvitationLetter::all();

        return view('invitationletter.index', compact('message', 'data'));

    }
}
