<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationPreferences extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    protected $table = 'invitation_preferences';

    public function preferencesitem(){
        return $this->belongsTo('App\Models\Preferences_item', 'preferences_item_id','id');
    }
}
