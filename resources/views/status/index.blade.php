@extends('templateblank')

@section('title')
Admin Invitation Hunter
@endsection

@section('headercode')

@endsection


@section('maincontent')



<div class="py-12 bg-white mx-auto mt-8 mb-8 w-5/6">
    <div class="max-w-6xl px-4 sm:px-6 lg:px-8">
        <div class="lg:text-center">
            <h2 class="text-base text-indigo-600 font-semibold tracking-wide uppercase">{{ENV('APP_NAME')}}</h2>
            <p class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                Invitation Status Code
            </p>
            <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                This is the status code reference of each invitation
            </p>
        </div>


        {{--  THE DATA GOES HERE  --}}
        <div class="mt-10 item-center">

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">

                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                            
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Id
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Status Name
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">

                                    @foreach ($data as $item)

                                    <tr>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">

                                            {{$item->id}}

                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {{$item->statusname}}
                                        </td>

                                        
                                    </tr>

                                    @endforeach


                                    <!-- More people... -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footercode')

@endsection