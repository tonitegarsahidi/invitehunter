

        @if (!empty($message))

            <div class="px-5 py-5 my-5 {{$message['color']}}">
                <em>
                    <?=$message['content']?>
                </em>
            </div>

        @endif
    

        @if ($errors->any())

            <div class="notification bg-red-200">
                <button class="delete jb-notification-dismiss"></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>

            </div>
        @endif
    