<?php

namespace Tests\Feature;

use App\Models\InvitationLetter;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvitationLetterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    public function test_invitation_letter_can_be_created()
    {
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        // $response->assertOk();

        $this->assertCount(1, InvitationLetter::all());
    }

    public function test_invitation_letter_can_be_updated(){

        // $this->withoutExceptionHandling();

        $this->post('/invl', [
            'email' => 'sultandaughter@huntstreet.com',
            'status_id' => '1   '
        ]);

        $invitationLetter = InvitationLetter::first();

        $response = $this->patch('/invl/'.$invitationLetter->id,[
             'email' => 'sultanwife@huntstreet.com',
             'status_id' => '2'
         ]);

        $this->assertEquals('sultanwife@huntstreet.com', InvitationLetter::first()->email);
        $this->assertEquals('2', InvitationLetter::first()->status_id);    
    }


    public function test_invitation_letter_can_be_updated_status_idonly(){

        // $this->withoutExceptionHandling();

        $this->post('/invl', [
            'email' => 'sultandaughter@huntstreet.com',
            'status_id' => '1'
        ]);

        $invitationLetter = InvitationLetter::first();

        $response = $this->patch('/invl/'.$invitationLetter->id,[
             'status_id' => '2'
         ]);

        $this->assertEquals('sultandaughter@huntstreet.com', InvitationLetter::first()->email);
        $this->assertEquals('2', InvitationLetter::first()->status_id);
        
    }

    public function test_invitation_letter_can_be_viewed(){

        $this->seed();

        $this->withoutExceptionHandling();

        $this->post('/invl', [
            'email' => 'sultandaughter@huntstreet.com',
            'status_id' => '2'
        ]);

        $invitationLetter = InvitationLetter::first();

        $response = $this->get('/invl/'.$invitationLetter->id);

        $this->assertEquals('sultandaughter@huntstreet.com', InvitationLetter::first()->email);
        $this->assertEquals('2', InvitationLetter::first()->status_id);

        $this->assertNotNull(InvitationLetter::first()->token);

        // dd($response);
        
    }

    public function test_invitation_letter_can_be_deleted(){
        $this->seed();

        $this->withoutExceptionHandling();

        $this->post('/invl', [
            'email' => 'sultandaughter@huntstreet.com',
        ]);

        $invitationLetter = InvitationLetter::first();

        $response = $this->delete('/invl/'.$invitationLetter->id);

        $this->assertCount(0, InvitationLetter::all());
        
    }


    public function test_posting_duplicate_email_wont_push_double(){
        $this->seed();

        // $this->withoutExceptionHandling();
        //FIRST
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();
        
        $this->assertCount(1, InvitationLetter::all());

        //POST AGAIN, same email    
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $this->assertCount(1, InvitationLetter::all());

    }

    public function test_valid_invitation_can_be_accessed(){
        $this->seed();

        $this->withoutExceptionHandling();
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();
        
        $this->assertCount(1, InvitationLetter::all());

        //get the current inserted token
        $currenttoken = InvitationLetter::first()->token;
        //test accessing
        $response = $this->get('/invitation/'.$currenttoken);

        $response->assertstatus(200);

    }

    function test_valid_invitation_contain_emails(){
        $this->seed();
        $this->withoutExceptionHandling();
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();
        
        $this->assertCount(1, InvitationLetter::all());

        //get the current inserted token
        $currenttoken = InvitationLetter::first()->token;
        //test accessing
        $response = $this->get('/invitation/'.$currenttoken);

        $response->assertSee('testmail@fakeemail.com');
    }

    public function test_invalid_invitation_will_result_404(){
        $this->seed();

        $this->withExceptionHandling();
        $response = $this->post('/invl', [
            'email' => 'testmail@fakeemail.com',
        ]);

        $response->assertOk();
        
        $this->assertCount(1, InvitationLetter::all());

        //get the current inserted token
        $currenttoken = InvitationLetter::first()->token;

        $newtoken = $currenttoken."XXX";

        //test accessing
        $response = $this->get('/invitation/'.$newtoken);

        $response->assertstatus(404);
    }



    
}
