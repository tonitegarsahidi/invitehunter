<?php

/**
 * DEFINISIKAN HAL-HAL YANG PERLU DICONFIG KAN TERKAIT  PROGRAM SIMOPT INI
 */

    return [

        'invitationsubject' => 'BazaarHunt Festival Exclusive Invitation',
        'confirmationsubject' => 'Thanks for register - We wait your arrival ',

        /* =======================================
        * CONFIG TERKAIT PANJANG TOKEN
        * =======================================
        */
        'tokenlength' => 10,
        
        /* =======================================
        * CONFIG ABOUT DEADLINE REGISTRATION
        * =======================================
        */
        'datedeadline' => '2021-11-2',

        /* =======================================
        * CONFIG ABOUT EVENT DATE
        * =======================================
        */
        'dateevent' => '2021-11-24',
        
        /* =======================================
        * CONFIG ABOUT DELAYED VERIFICATION EMAIL IN MINUTES
        * =======================================
        */
        'delayemail' => 60,
    



    ]

?>