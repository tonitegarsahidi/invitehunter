<?php

namespace Tests\Feature;

use App\Models\Preferences_item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PreferencesItemTest extends TestCase
{
    use RefreshDatabase;    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_preference_item_can_be_created()
    {
        $this->withoutExceptionHandling();
        $response = $this->post('/preferencesitem', [
            'itemname' => 'Zaheedee',
        ]);

        $response->assertOk();

        $this->assertCount(1, Preferences_item::all());
    }
}
