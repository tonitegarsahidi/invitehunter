<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\InvitationLetterController;
use App\Http\Controllers\InvitationDataController;
use App\Http\Controllers\PreferencesItemController;
use App\Http\Controllers\StatusController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
    // return view('welcome');
});

Route::post('/invl', [invitationLetterController::class, 'store'])->name('INVITATIONLETTER.create');
Route::get('/invl', [invitationLetterController::class, 'index'])->name('INVITATIONLETTER.index');
Route::get('/invl/confirm', [invitationLetterController::class, 'indexconfirm'])->name('INVITATIONLETTER.confirm');
Route::get('/invl/sendallinvitation', [invitationLetterController::class, 'sendAllInvitation'])->name('INVITATIONLETTER.sendallinvitation');
Route::get('/invl/create', [invitationLetterController::class, 'create'])->name('INVITATIONLETTER.create');
Route::get('/invl/{invitationLetter}', [invitationLetterController::class, 'show'])->name('INVITATIONLETTER.show');
Route::patch('/invl/{invitationLetter}', [invitationLetterController::class, 'update'])->name('INVITATIONLETTER.update');

Route::delete('/invl/{invitationLetter}', [invitationLetterController::class, 'destroy'])->name('INVITATIONLETTER.delete');

Route::get('/invitation/{token}', [invitationLetterController::class, 'showinvitation'])->name('INVITATIONLETTER.showinvitation');
Route::get('/invitation/{token}/thankyou', [invitationLetterController::class, 'showinvitation'])->name('INVITATIONLETTER.showinvitation');

//
Route::post('/invitation/{token}', [invitationDataController::class, 'store'])->name('INVITATIONDATA.accept');


Route::get('/admin', [AdminController::class, 'index'])->name('ADMIN.index');

Route::get('/status', [StatusController::class, 'index'])->name('STATUS.index');

//preferenve item,e.g. brand, fav food, etc.
Route::get('/preferencesitem', [PreferencesItemController::class, 'index'])->name('PREFERENCEITEM.index');
Route::post('/preferencesitem', [PreferencesItemController::class, 'store'])->name('PREFERENCEITEM.create');