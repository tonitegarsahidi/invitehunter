
<h1 id="app" class="text-3xl">
    @{{message}}
</h1>

<script>
    var app = new Vue({
        el: '#app',
        data: {
          message: 'Hello Vue!'
        }
      })


      app.message = "loading time";

      $(document).ready(function(){    
        setInterval(function(){
            let hour = 0;
            let minute = 0;
            let second = 0;
            let day = 0;
            let month = 0;

            let timenow = Date.now();
            let datetarget = Date.parse("{{$datetarget}}");

            let timedifference = datetarget - timenow;

            timedifference /= 1000; //(ignore milisecond);

            day = Math.floor(timedifference / (24*60*60));
            timedifference -= (day * 24 * 60 * 60);

            hour = Math.floor(timedifference / (60*60));
            timedifference -= (hour * 60 * 60);
            
            minute = Math.floor(timedifference / (60));
            timedifference -= (minute * 60);

            second = Math.floor(timedifference);



            let text = `${day} days ${hour} : ${minute} : ${second}`;
            app.message = text;
        },1000); 
     });
</script>