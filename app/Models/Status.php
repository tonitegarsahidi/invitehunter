<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "status";

    public function invitationLetter(){
        return $this->hasMany('App\Models\InvitationLetter', 'status_id','id');
    }


}
