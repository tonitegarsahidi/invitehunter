<?php

namespace Database\Seeders;

use App\Models\InvitationLetter;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
           Preference_item_seeder::class,
           StatusSeeder::class,
        //    InvitationLetterSeeder::class,
        ]);
        
    }
}
