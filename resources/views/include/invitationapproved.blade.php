<hr>
<table class="mx-2 my-2">
    <tr>
        <th class="py-2 px-2">Name</th>
        <td class="py-2 px-2"> : {{$data->invitationdata->name}}</td>
    </tr>
    <tr>
        <th class="py-2 px-2">Email</th>
        <td class="py-2 px-2"> : {{$data->email}}</td>
    </tr>
</table>
<h3>This is your pass Key</h3>
<div class="decoration-clone bg-gradient-to-b from-yellow-400 to-white-500 text-green-800 text-2xl text-center">
    <code>{{$data->key}}</code>
</div>

<pre class="text-sm">
    Please show a valid ID and key above on entrance. 
</pre>
<br>
<br>
<hr>

<div class="bg-green-200 text-center">
    @php
        $datetarget = config('invitationhunter.dateevent');


        $timestamp = strtotime($datetarget);
        $datedisplay = date("l d F Y", $timestamp);
    @endphp

    <h3>Countdown to Event Date ({{$datedisplay}})</h3>


    @include('include/countdowntimer')
</div>