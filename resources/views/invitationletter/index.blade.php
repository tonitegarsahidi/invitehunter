@extends('templateblank')

@section('title')
Admin Invitation Hunter
@endsection

@section('headercode')

@endsection


@section('maincontent')



<div class="py-12 bg-white mx-auto mt-8 mb-8 w-5/6">
    <div class="max-w-6xl px-4 sm:px-6 lg:px-8">
        <div class="lg:text-center">
            <h2 class="text-base text-indigo-600 font-semibold tracking-wide uppercase">{{ENV('APP_NAME')}}</h2>
            <p class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                Invitation List
            </p>
            <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                Below are List of all available prospects
            </p>
        </div>


        {{--  THE DATA GOES HERE  --}}
        <div class="mt-10 item-center">

            <a href="/invl/create" class="button">
                <button class="px-2 py-2 mx-2 my-2 bg-blue-200">
                    Add New Invitation
                </button>
            </a>
            
            <a href="/invl/sendallinvitation" class="button">
                <button class="px-2 py-2 mx-2 my-2 bg-yellow-200">
                    Send All Invitation
                </button>
            </a>

            @include('include/message')

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">

                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

                            
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Email
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Token
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Status
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Last Update
                                        </th>
                                        <th scope="col" class="relative px-6 py-3">
                                            <span class="sr-only">Delete</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">

                                    @foreach ($data as $item)

                                    <tr>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">

                                            {{$item->email}}

                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <a href="/invitation/{{$item->token}}" target="_blank" rel="noopener noreferrer">
                                            {{$item->token}}
                                            </a>
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap">

                                            
                                           
                                            @if ($item->status_id == 1)
                                                <span
                                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-100 text-yellow-800">
                                                    {{$item->status->statusname}}
                                                </span>
                                                
                                            @elseif ($item->status_id == 2)
                                                <span
                                                class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-blue-100 text-blue-800">
                                                {{$item->status->statusname}}
                                                </span>
                                                
                                            @else
                                                <span
                                                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                    {{$item->status->statusname}}
                                                </span>
                                                
                                            @endif

                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            {{$item->updated_at}}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            @if ($item->status_id == 1)
                                                <form action="/invl/{{$item->id}}" method="post">
                                                    @method('delete')
                                                    @csrf

                                                    <button type="submit">Delete</button>
                                                </form>
                                                
                                            @endif
                                            
                                        </td>
                                    </tr>

                                    @endforeach


                                    <!-- More people... -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footercode')

@endsection